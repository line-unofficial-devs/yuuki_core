Yuuki_Core
==========
The LINE Protocol for Star Yuuki BOT

https://gitlab.com/star-inc/yuuki_core

    This Source Code Form is subject to the terms of the Mozilla Public
    License, v. 2.0. If a copy of the MPL was not distributed with this
    file, You can obtain one at http://mozilla.org/MPL/2.0/.

    This Source Code Form is "Incompatible With Secondary Licenses", as
    defined by the Mozilla Public License, v. 2.0.

Copyright 2020 Star Inc.(https://starinc.xyz) All Rights Reserved.
